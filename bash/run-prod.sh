#!/usr/bin/env bash
yes yes | export -p > /tmp/env.txt && python3 manage.py migrate && python3 manage.py collectstatic --noinput && service cron start && env > /etc/environment && echo "*/5 * * * * /usr/local/bin/python3 /app/manage.py runcrons"  > /tmp/cron.txt && crontab /tmp/cron.txt && gunicorn sos_laveries.wsgi -b 0.0.0.0:8000 --log-file -
