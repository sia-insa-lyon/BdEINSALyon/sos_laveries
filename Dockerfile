FROM python:3.8

EXPOSE 8000

RUN apt-get update && apt-get install -y cron

WORKDIR /app
COPY requirements.txt /app
RUN pip3 install -r requirements.txt
COPY . /app
RUN mkdir /app/tmp

ENV DATABASE_URL postgres://postgresql@postgres/laveries
ENV SECRET_KEY 'ze-super-cle-secrete'
ENV DJANGO_ENV 'dev'
ENV MAILGUN_KEY ''
ENV MAILGUN_DOMAIN ''
ENV DEFAULT_FROM_EMAIL ''
ENV HCAPTCHA_PUBLIC_KEY ''
ENV HCAPTCHA_PRIVATE_KEY ''
ENV EMAIL_RESP_SERVICES 'services@bde-insa-lyon.fr'
ENV EMAIL_RESP_LAVERIE ''

VOLUME /app/staticfiles

RUN chmod +x /app/bash/run-prod.sh
CMD /app/bash/run-prod.sh
