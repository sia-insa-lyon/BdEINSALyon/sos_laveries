# Laveries Helm Chart

## Values

### Values

| Value | Default | Comment |
|:------|---------|---------|
| `image.repository`|registry.gitlab.com/sia-insa-lyon/bdeinsalyon/sos_laveries:latest  |         |
|`image.pullPolicy`|Always||
|`default_from_email`|"Laveries BdE INSA \<sos-laveries@mg.bde-insa-lyon.fr\>"|Email address to send mail with mailjet|
|`email_resp_laverie`|"laveries@bde-insa-lyon.fr"|Email address where the notification emails will be send|
|`email_resp_services`|"services@bde-insa-lyon.fr"|Email address where the notification emails will be send|
|`resources.django.requests.cpu`|300m||
|`resources.django.requests.memory`|500mi||
|`resources.django.limits.cpu`|500m||
|`resources.django.limits.memory`|1Gi||
|`resources.nginx.requests.cpu`|300m||
|`resources.nginx.requests.memory`|200mi||
|`resources.nginx.limits.cpu`|500m||
|`resources.nginx.limits.memory`|500Gi||
|`autoscaling.enabled`|false||
|`autoscaling.minReplicas`|1||
|`autoscaling.maxReplicas`|5||
|`autoscaling.targetCPUUtilizationPercentage`|90||
|`ingress.enabled`|false||
|`ingress.host`|null||
|`service.type`|NodePort|NodePort, ClusterIP, LoadBalancer|
|`secret.mailjetApiKey`|"v3rysecr3t"|API Key used to mailjet |
|`mailjetSecretKey`|"v3rysecr3t"|Secret Key used to mailjet|
|`djangoSecretKey`|"v3rysecr3t"|Secret Key used by django (must be long)|
|`hcaptcha_public_key`|"v3rysecr3t"|Public Key used to hcaptcha|
|`hcaptcha_private_key`|"v3rysecr3t"|Private Key used to hcaptcha|

### External Database Values

If you use External Database (postgresql).

| Value | Default | Comment |
|:------|---------|---------|
|`externalDatabase.enabled`|false|Set at true if you will used external Database|
|`externalDatabase.host`|||
|`externalDatabase.database`|||
|`externalDatabase.port`|||
|`externalDatabase.username`|||
|`externalDatabase.password`|||

### Database Values

If you don't use External Database.

| Value | Default | Comment |
|:------|---------|---------|
|`postgresql.enabled`|true|Set at true if you will used postgresql Database|
|`postgresql.postgresqlPassword`|password||
|`postgresql.postgresqlDatabase`|django||
|`postgresql.persistence.accessModes`|[ReadWriteMany]||
|`postgresql.persistence.size`|1Gi||
|`postgresql.volumePermissions.enabled`|true||

### Backup Values

Backup database postgresql in object storage ([link]("https://gitlab.com/sia-insa-lyon/dev/postgres-backup-container")). This script can be used if you don't used externalDatabase.
